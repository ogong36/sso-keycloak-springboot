package com.example.app1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class App1Application {

	@RequestMapping("/hello")
	public String hello() {
		return "Hello, Keycloak! This is APP-1";
	}

	@RequestMapping("/app1")
	public String tracingTest() {
		return "This is permitAll!(APP-1)";
	}

	public static void main(String[] args) {
		SpringApplication.run(App1Application.class, args);
	}
}
