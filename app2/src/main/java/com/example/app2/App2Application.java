package com.example.app2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class App2Application {

	@RequestMapping("/hello")
	public String hello() {
		return "Hello, Keycloak! This is APP-2";
	}

	@RequestMapping("/app2")
	public String tracingTest() {
		return "This is permitAll!(APP-2)";
	}

	public static void main(String[] args) {
		SpringApplication.run(App2Application.class, args);
	}
}
